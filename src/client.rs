use std::{
    collections::HashMap,
    time::Duration,
    thread
};
use http;
use url_enc;
use ::error::{Result, Error::AuthenticationError};
use ::config::Config;
use ::auth::{AuthUrl, AuthQuery, OAuthToken};


#[derive(Debug)]
pub struct Client {
    config: Config,
}


impl Client {
    pub fn new<S>(consumer_key: S, consumer_secret: S, username: S, password: S) -> Result<Self>
        where S: Into<String>
    {
        let config = Config::new(consumer_key, consumer_secret, username, password)?;
        Ok(Self { config })
    }

    pub fn login(&self) -> Result<OAuthToken> {
        let urls = &self.config.urls.auth;
        let consumer = &self.config.secrets.consumer;
        let credentials = &self.config.secrets.user;
        let http_client = http::Client::new();

        let url = AuthUrl::new(urls.get_tmp_token.clone(), &consumer);
        let resp = http_client.get(url.sign()).send()?;
        let tmp_token: OAuthToken = url_enc::from_reader(resp)?;
        thread::sleep(Duration::from_micros(500));

        let mut query = AuthQuery::new(&consumer.key);
        query.username(&credentials.name);
        query.password(&credentials.password);
        let mut url = AuthUrl::with_query(urls.authorize_tmp_token.clone(), query, &consumer);
        url.token(&tmp_token);
        let resp = http_client.get(url.sign()).send()?;
        let tmp: HashMap<String, String> = url_enc::from_reader(resp)?;
        let oauth_verifier = tmp.get("oauth_verifier").ok_or(AuthenticationError)?;
        thread::sleep(Duration::from_micros(500));

        let mut query = AuthQuery::new(&consumer.key);
        query.verifier(oauth_verifier);
        let mut url = AuthUrl::with_query(urls.get_token.clone(), query, &consumer);
        url.token(&tmp_token);
        let resp = http_client.get(url.sign()).send()?;
        let oauth_token: OAuthToken = url_enc::from_reader(resp)?;

        Ok(oauth_token)
    }
}
