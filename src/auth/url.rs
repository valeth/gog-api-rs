#![allow(dead_code)] // TODO: remove me!

use url::Url;
use url_enc;
use chrono::prelude::Utc;

use super::{
    OAuthToken,
    Consumer,
    utils::{
        create_signature,
        percencode,
        nonce
    }
};


#[derive(Debug, Serialize)]
pub(crate) struct AuthQuery<'a> {
    oauth_consumer_key: &'a str,
    oauth_nonce: String,
    oauth_signature_method: &'static str,
    oauth_timestamp: u64,
    oauth_token: Option<&'a str>,
    oauth_verifier: Option<&'a str>,
    oauth_version: &'static str,
    password: Option<String>,
    username: Option<String>,
}

impl<'a> AuthQuery<'a> {
    pub fn new(consumer_key: &'a str) -> Self {
        Self {
            oauth_consumer_key: consumer_key,
            oauth_nonce: nonce(22),
            oauth_signature_method: "HMAC-SHA1",
            oauth_timestamp: Utc::now().timestamp() as u64,
            oauth_token: None,
            oauth_verifier: None,
            oauth_version: "1.0",
            password: None,
            username: None
        }
    }

    pub fn token(&mut self, tok: &'a str) {
        self.oauth_token = Some(tok);
    }

    pub fn verifier(&mut self, ver: &'a str) {
        self.oauth_verifier = Some(ver);
    }

    pub fn password(&mut self, pass: &'a str) {
        self.password = Some(percencode(pass));
    }

    pub fn username(&mut self, name: &'a str) {
        self.username = Some(percencode(name));
    }
}


#[derive(Debug)]
pub(crate) struct AuthUrl<'a> {
    base: Url,
    query: AuthQuery<'a>,
    consumer: &'a Consumer,
    token: Option<&'a OAuthToken>,
}

impl<'a> AuthUrl<'a> {
    pub fn new(base: Url, consumer: &'a Consumer) -> Self {
        let query = AuthQuery::new(&consumer.key);
        Self::with_query(base, query, consumer)
    }

    pub fn with_query(base: Url, query: AuthQuery<'a>, consumer: &'a Consumer) -> Self {
        Self { token: None, query, base, consumer }
    }

    pub fn token(&mut self, tok: &'a OAuthToken) {
        self.token = Some(tok);
        self.query.token(&tok.key);
    }

    pub fn sign(&self) -> Url {
        let mut signed = self.base.clone();
        signed.set_query(
            Some(&format!("{}&oauth_signature={}", url_enc::to_string(&self.query).unwrap(), self.signature()))
        );
        signed
    }

    fn signature(&self) -> String {
        let base = self.signature_base();
        let sigkey = self.signing_key();
        create_signature(base, sigkey)
    }

    fn signature_base(&self) -> String {
        let query_string = url_enc::to_string(&self.query).unwrap();
        format!("GET&{}&{}", percencode(&self.base), percencode(&query_string))
    }

    fn signing_key(&self) -> String {
        let consumer = &self.consumer;
        let token_secret = match self.token {
            Some(s) => percencode(s.secret.clone()),
            None => "".to_owned()
        };
        format!("{}&{}", percencode(&consumer.secret), token_secret)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_oauth_initialize() {
        let consumer = Consumer {
            key: "abc123".to_owned(),
            secret: "zxyuiab12".to_owned()
        };
        let mut query = AuthQuery::new(&consumer.key);

        // set a fixed timestamp and nonce
        query.oauth_timestamp = 1532729335;
        query.oauth_nonce = "XcPIvGHBd1wcZiZBGdnOS9".to_string();

        let url = AuthUrl::with_query(
            Url::parse("https://api.gog.com/oauth/initialize").unwrap(),
            query, &consumer
        );

        assert_eq!(url.signing_key(), "zxyuiab12&");
        assert_eq!(
            url.signature_base(),
            "GET\
            &https%3A%2F%2Fapi.gog.com%2Foauth%2Finitialize\
            &oauth_consumer_key%3Dabc123\
            %26oauth_nonce%3DXcPIvGHBd1wcZiZBGdnOS9\
            %26oauth_signature_method%3DHMAC-SHA1\
            %26oauth_timestamp%3D1532729335\
            %26oauth_version%3D1.0"
        );
        assert_eq!(
            url.sign(),
            Url::parse(
                "https://api.gog.com/oauth/initialize\
                ?oauth_consumer_key=abc123\
                &oauth_nonce=XcPIvGHBd1wcZiZBGdnOS9\
                &oauth_signature_method=HMAC-SHA1\
                &oauth_timestamp=1532729335\
                &oauth_version=1.0\
                &oauth_signature=jIYBYCi4ntGscBnJHW%2B7eRJPUYM%3D"
            ).unwrap()
        );
    }

    #[test]
    fn test_oauth_login() {
        let consumer = Consumer {
            key: "abc123".to_owned(),
            secret: "zxyuiab12".to_owned()
        };
        let oauth_token = OAuthToken {
            key: "b76bc4514916a1271f59c3df9c135efae5205ddc".to_owned(),
	        secret: "d6dab87cde2f969a2aa08289828f4ab2f79b66a8".to_owned()
        };
        let mut query = AuthQuery::new(&consumer.key);

        // set a fixed timestamp and nonce
        query.oauth_timestamp = 1532793935;
        query.oauth_nonce = "MB_O_x266eRSG7mGmkUnRn".to_string();

        query.username("test@example.com");
        query.password("123456");

        let mut url = AuthUrl::with_query(
            Url::parse("https://api.gog.com/oauth/login").unwrap(),
            query, &consumer
        );

        url.token(&oauth_token);

        assert_eq!(url.signing_key(), "zxyuiab12&d6dab87cde2f969a2aa08289828f4ab2f79b66a8");
        assert_eq!(
            url.signature_base(),
            "GET\
            &https%3A%2F%2Fapi.gog.com%2Foauth%2Flogin\
            &oauth_consumer_key%3Dabc123\
            %26oauth_nonce%3DMB_O_x266eRSG7mGmkUnRn\
            %26oauth_signature_method%3DHMAC-SHA1\
            %26oauth_timestamp%3D1532793935\
            %26oauth_token%3Db76bc4514916a1271f59c3df9c135efae5205ddc\
            %26oauth_version%3D1.0\
            %26password%3D123456\
            %26username%3Dtest%2540example.com"
        );
        assert_eq!(
            url.sign(),
            Url::parse(
                "https://api.gog.com/oauth/login\
                ?oauth_consumer_key=abc123\
                &oauth_nonce=MB_O_x266eRSG7mGmkUnRn\
                &oauth_signature_method=HMAC-SHA1\
                &oauth_timestamp=1532793935\
                &oauth_token=b76bc4514916a1271f59c3df9c135efae5205ddc\
                &oauth_version=1.0\
                &password=123456\
                &username=test%40example.com\
                &oauth_signature=tVlXMGzYUz288iAlJWU6BtZ%2By4s%3D"
            ).unwrap()
        );
    }

    #[test]
    fn test_oauth_get_token() {
        let consumer = Consumer {
            key: "abc123".to_owned(),
            secret: "zxyuiab12".to_owned()
        };
        let oauth_token = OAuthToken {
	        key: "b76bc4514916a1271f59c3df9c135efae5205ddc".to_owned(),
	        secret: "d6dab87cde2f969a2aa08289828f4ab2f79b66a8".to_owned()
        };
        let mut query = AuthQuery::new(&consumer.key);

        // set a fixed timestamp and nonce
        query.oauth_timestamp = 1532795571;
        query.oauth_nonce = "ZaeZMizIE6Z97uEn7ziBc".to_string();
        query.verifier("a21c288ef218ab");

        let mut url = AuthUrl::with_query(
            Url::parse("https://api.gog.com/oauth/token").unwrap(),
            query, &consumer
        );

        url.token(&oauth_token);

        assert_eq!(url.signing_key(), "zxyuiab12&d6dab87cde2f969a2aa08289828f4ab2f79b66a8");
        assert_eq!(
            url.signature_base(),
            "GET\
            &https%3A%2F%2Fapi.gog.com%2Foauth%2Ftoken\
            &oauth_consumer_key%3Dabc123\
            %26oauth_nonce%3DZaeZMizIE6Z97uEn7ziBc\
            %26oauth_signature_method%3DHMAC-SHA1\
            %26oauth_timestamp%3D1532795571\
            %26oauth_token%3Db76bc4514916a1271f59c3df9c135efae5205ddc\
            %26oauth_verifier%3Da21c288ef218ab\
            %26oauth_version%3D1.0"
        );
        assert_eq!(
            url.sign(),
            Url::parse(
                "https://api.gog.com/oauth/token\
                ?oauth_consumer_key=abc123\
                &oauth_nonce=ZaeZMizIE6Z97uEn7ziBc\
                &oauth_signature_method=HMAC-SHA1\
                &oauth_timestamp=1532795571\
                &oauth_token=b76bc4514916a1271f59c3df9c135efae5205ddc\
                &oauth_verifier=a21c288ef218ab\
                &oauth_version=1.0\
                &oauth_signature=ecG0XeSqCExq6Qxsi4TUGx3x%2Bb4%3D"
            ).unwrap()
        );
    }
}
