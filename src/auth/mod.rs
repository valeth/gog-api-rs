mod utils;
mod url;


pub(crate) use self::url::{
    AuthUrl,
    AuthQuery
};


#[derive(Debug, Deserialize)]
pub struct OAuthToken {
    #[serde(rename = "oauth_token")]
    key: String,
    #[serde(rename = "oauth_token_secret")]
    secret: String,
}

#[derive(Debug, PartialEq)]
pub(crate) struct Consumer {
    pub key: String,
    pub secret: String,
}
