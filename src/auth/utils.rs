use percent_encoding::{
    utf8_percent_encode,
    SIMPLE_ENCODE_SET
};
use sha1::Sha1;
use hmac::{
    Hmac,
    Mac
};
use base64;
use rand::{
    Rng,
    thread_rng,
    distributions as dists
};


type HmacSha1 = Hmac<Sha1>;


define_encode_set! {
    pub OAUTH_ENCODE_SET = [SIMPLE_ENCODE_SET] | { '/', ':', '&', '=', '+', '%' }
}


pub fn percencode<S>(string: S) -> String
    where S: AsRef<str>
{
    utf8_percent_encode(string.as_ref(), OAUTH_ENCODE_SET).to_string()
}

pub fn create_signature(base: String, signing_key: String) -> String {
    let mut mac = HmacSha1::new_varkey(signing_key.as_bytes()).unwrap();
    mac.input(base.as_bytes());
    let result = mac.result();
    let code = result.code();

    let sig = base64::encode(code.as_slice());

    percencode(sig)
}

pub fn nonce(length: usize) -> String {
    let mut rng = thread_rng();
    rng.sample_iter(&dists::Alphanumeric).take(length).collect::<String>()
}
