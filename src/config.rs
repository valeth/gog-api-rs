use std::result;
use url::Url;
use json;
use serde::{Deserializer, Deserialize};
use http;
use ::error::Result;
use ::auth::Consumer;


const CONFIG_URL: &'static str = "https://api.gog.com/downloader2/status/stable/";


fn deserialize_url<'de, D>(deserializer: D) -> result::Result<Url, D::Error>
    where D: Deserializer<'de>
{
    let deserialized = String::deserialize(deserializer)?;
    let url = Url::parse(&deserialized).unwrap();
    Ok(url)
}


#[derive(Debug, PartialEq, Deserialize)]
pub(crate) struct UserUrls {
    #[serde(rename = "get_user_games", deserialize_with = "self::deserialize_url")]
    pub get_games: Url,
    #[serde(rename = "get_user_details", deserialize_with = "self::deserialize_url")]
    pub get_details: Url,
}

#[derive(Debug, PartialEq, Deserialize)]
pub(crate) struct AuthUrls {
    #[serde(rename = "oauth_authorize_temp_token", deserialize_with = "self::deserialize_url")]
    pub authorize_tmp_token: Url,
    #[serde(rename = "oauth_get_temp_token", deserialize_with = "self::deserialize_url")]
    pub get_tmp_token: Url,
    #[serde(rename = "oauth_get_token", deserialize_with = "self::deserialize_url")]
    pub get_token: Url,
}

#[derive(Debug, PartialEq, Deserialize)]
pub(crate) struct Urls {
    #[serde(flatten)]
    pub auth: AuthUrls,

    #[serde(flatten)]
    pub user: UserUrls,

    #[serde(deserialize_with = "self::deserialize_url")]
    pub get_installer_link: Url,
    #[serde(deserialize_with = "self::deserialize_url")]
    pub get_game_details: Url,
    #[serde(deserialize_with = "self::deserialize_url")]
    pub get_extra_link: Url,
    #[serde(deserialize_with = "self::deserialize_url")]
    pub set_app_status: Url,
}

#[derive(Debug, PartialEq)]
pub(crate) struct UserCredentials {
    pub name: String,
    pub password: String,
}

#[derive(Debug, PartialEq)]
pub(crate) struct Secrets {
    pub consumer: Consumer,
    pub user: UserCredentials
}


#[derive(Debug, PartialEq)]
pub(crate) struct Config {
    pub urls: Urls,
    pub secrets: Secrets
}


impl Config {
    pub fn new<S>(consumer_key: S, consumer_secret: S, username: S, password: S) -> Result<Config>
        where S: Into<String>
    {
        let urls = Self::fetch_config_urls()?;
        let secrets = Secrets {
            consumer: Consumer {
                key: consumer_key.into(),
                secret: consumer_secret.into()
            },
            user: UserCredentials {
                name: username.into(),
                password: password.into()
            }
        };
        Ok(Config { urls, secrets })
    }

    /// Fetch and serialize configuration
    fn fetch_config_urls() -> Result<Urls> {
        let http_client = http::Client::new();
        let mut response = http_client.get(CONFIG_URL).send()?;
        let mut tmp: json::Value = response.json()?;
        let cfg = json::from_value(tmp["config"].take())?;
        Ok(cfg)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! gog_url {
        [ config: $path:expr ] => { gog_url!(&format!("downloader2/{}", $path)) };
        [ auth: $path:expr ]   => { gog_url!(&format!("oauth/{}", $path)) };
        [ $path:expr ]         => {
            Url::parse("https://api.gog.com")
                .expect("Invalid base URL")
                .join($path)
                .expect("Invalid path for URL")
        }
    }

    #[test]
    fn test_fetch_config_urls() {
        let json_response = r###"
            {
                "oauth_authorize_temp_token": "https://api.gog.com/oauth/login",
                "oauth_get_temp_token":       "https://api.gog.com/oauth/initialize",
                "oauth_get_token":            "https://api.gog.com/oauth/token",
                "get_user_games":             "https://api.gog.com/downloader2/user_games",
                "get_user_details":           "https://api.gog.com/downloader2/user",
                "get_installer_link":         "https://api.gog.com/downloader2/installer",
                "get_game_details":           "https://api.gog.com/downloader2/game",
                "get_extra_link":             "https://api.gog.com/downloader2/extra",
                "set_app_status":             "https://api.gog.com/downloader2/set_app_status"
            }
        "###;

        let expected_config_urls = Urls {
            auth: AuthUrls {
                authorize_tmp_token: gog_url!(auth: "login"),
                get_tmp_token:       gog_url!(auth: "initialize"),
                get_token:           gog_url!(auth: "token"),
            },
            user: UserUrls {
                get_games:   gog_url!(config: "user_games"),
                get_details: gog_url!(config: "user"),
            },
            get_installer_link: gog_url!(config: "installer"),
            get_game_details:   gog_url!(config: "game"),
            get_extra_link:     gog_url!(config: "extra"),
            set_app_status:     gog_url!(config: "set_app_status")
        };
        let urls: Urls = json::from_str(json_response).unwrap();
        assert_eq!(expected_config_urls, urls);
    }
}
