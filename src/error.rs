use std::{
    result,
    io
};
use json;
use http;
use url_enc;


pub type Result<T> = result::Result<T, Error>;


#[derive(Debug)]
pub enum Error {
    AuthenticationError,
    IoError(io::Error),
    JsonError(json::Error),
    HttpError(http::Error),
    UrlDecodingError(url_enc::de::Error),
}


impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::IoError(err)
    }
}

impl From<json::Error> for Error {
    fn from(err: json::Error) -> Self {
        Error::JsonError(err)
    }
}

impl From<http::Error> for Error {
    fn from(err: http::Error) -> Self {
        Error::HttpError(err)
    }
}

impl From<url_enc::de::Error> for Error {
    fn from(err: url_enc::de::Error) -> Self {
        Error::UrlDecodingError(err)
    }
}
