extern crate kankyo as dotenv;
extern crate gog_api;

use std::env;
use gog_api::Client;

fn main() {
    dotenv::load().expect("Failed to load environment file");
    let consumer_key = env::var("GOG_CONSUMER_KEY").expect("Missing consumer key");
    let consumer_secret = env::var("GOG_CONSUMER_SECRET").expect("Missing consumer secret");
    let username = env::var("GOG_USER_EMAIL").expect("Missing user email (username)");
    let password = env::var("GOG_USER_PASSWORD").expect("Missing user password");

    let client = Client::new(consumer_key, consumer_secret, username, password).unwrap();

    println!("{:#?}", client);

    let oauth_token = client.login().unwrap();
    println!("OAuth Token: {:?}", oauth_token);
}
